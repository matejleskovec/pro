#include <iostream>
#include <memory>
#include <vector>
#include <string>


std::vector<int>* input_get_line();
int ** create_matrix(int, int);
void delete_matrix(int**,int);

int main()
{
	int apple_tree_count, trees_per_day, days_to_pick;
	int* apples_per_day_from_tree;
	std::vector<int>* apples_per_tree;
	int** matrix;

	//Input line 1
	auto input_vector = input_get_line();
	if (input_vector->size() != 3) {
		std::cout << "Err: line 1 input count: " << input_vector->size() << std::endl;
	}
	
	apple_tree_count = input_vector->at(0);
	trees_per_day = input_vector->at(1);
	days_to_pick = input_vector->at(2);
	delete input_vector;
	
		
	//Input line 2
	apples_per_tree = input_get_line();
	if (apples_per_tree->size() != apple_tree_count) {
		std::cout << "Err: line 2 input count: " << apples_per_tree->size() << std::endl;
	}

	matrix =  create_matrix(apple_tree_count, days_to_pick);
	apples_per_day_from_tree = new int[apple_tree_count];
	int apple_tree_count_difference = apple_tree_count;
	for (int day = 0; day < days_to_pick; day++) {
		int max_apples = 0,count;
		for (int tree = apple_tree_count_difference - 1; tree >= 0; tree--) {
			count = 0;

			//Check apples
			if (day == 0) {
				for (int i = 0; i < trees_per_day; i++) {
					if (tree + i >= apple_tree_count_difference) break;
					count += apples_per_tree->at(tree + i);
				}
				apples_per_day_from_tree[tree] = count;
			}
			else {
				count += apples_per_day_from_tree[tree];
			}
			
			//Check next day
			if (day != 0 && (tree + trees_per_day) < apple_tree_count_difference) {
				count += matrix[tree + trees_per_day][day - 1];
			}

			//Check if posible value is greater
			if (count > max_apples) {
				matrix[tree][day] = count;
				max_apples = count;
			}
			else {
				matrix[tree][day] = max_apples;
			}
		}
	}
	std::cout << matrix[0][days_to_pick - 1] << std::endl;

	delete_matrix(matrix, apple_tree_count);
	delete apples_per_tree;
	return 0;
}

std::vector<int>* input_get_line()
{
	std::vector<int>* line_array = new std::vector<int>();
	int counter = 0, end = 0;
	std::string line;
	std::getline(std::cin >> std::ws, line);

	while (true) {
		end = line.find(" ");
		auto test = line.substr(0, end);
		if (end == std::string::npos) {
			if (line != "") {
				line_array->push_back(std::stoi(line));
			}
			break;
		}
		else {
			line_array->push_back(std::stoi(line));
			counter++;
		}
		line = line.substr(end + 1);
	}
	return line_array;
}

int ** create_matrix(int x, int y)
{
	int** matrix = new int*[x];
	for (int i = 0; i < x; i++) {
		matrix[i] = new int[y];
	}
	return matrix;
}

void delete_matrix(int **matrix,int x)
{
	for (int i = 0; i < 2; i++) {
		delete[] matrix[i];
	}
	delete[] matrix;
}
